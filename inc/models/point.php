<?php namespace EOPlugin\Inc\Models;

use EOLib\Classes\Model\PluginModel;

/**
 * Class Points
 * @package EOPlugin\Inc\Models
 */
class Point extends PluginModel {

	/**
	 * @var string
	 */
	protected static $version = '1.0.0';

	/**
	 * @var string
	 */
	protected static $tableName = 'eo_wc_points';

	/**
	 * @var string
	 */
	protected static $primaryKey = 'id';

	/**
	 * @var array
	 */
	protected static $columns = [
		[
			'name'    => 'user_id',
			'type'    => 'bigint(20)',
			'default' => 'NOT NULL'
		],
		[
			'name'    => 'order_id',
			'type'    => 'bigint(20)',
			'default' => 'NOT NULL'
		],
		[
			'name'    => 'points',
			'type'    => 'bigint(20)',
			'default' => 'NOT NULL'
		],
		[
			'name'    => 'date',
			'type'    => 'datetime',
			'default' => 'NOT NULL'
		],
	];


	/**
	 * @param      $user_id
	 * @param      $points
	 * @param      $order_id
	 *
	 * @return bool
	 */
	public static function increasePoints( $user_id, $points, $order_id ) {

		global $wpdb;

		if ( ! $user = get_userdata( $user_id ) ) {
			return false;
		}

		$data = [
			'data'   => [
				'user_id'  => $user_id,
				'points'   => $points,
				'order_id' => $order_id,
				'date'     => current_time( 'mysql', 1 ),
			],
			'format' => [
				'%d',
				'%d',
				'%d',
				'%s',
			]
		];

		$success = $wpdb->insert(
			$wpdb->prefix . self::getTableName(),
			$data['data'],
			$data['format']
		);

		if ( ! $success ) {
			return false;
		}

		update_user_meta( $user_id,
			'eo_wc_points',
			(int) get_user_meta( $user_id, 'eo_wc_points', true ) + $points
		);

		return true;
	}

	/**
	 * @param      $user_id
	 * @param      $points
	 * @param      $event_type
	 * @param null $data
	 * @param null $order_id
	 *
	 * @return bool
	 */
	public static function decreasePoints( $user_id, $points, $order_id ) {

		global $wpdb;

		if ( ! $user = get_userdata( $user_id ) ) {
			return false;
		}

		$user_points = self::getUsersPoints( $user_id );

		if ( $user_points ) {

			$data = [
				'data'   => [
					'user_id'  => $user_id,
					'points'   => - $points,
					'order_id' => $order_id,
					'date'     => current_time( 'mysql', 1 ),
				],
				'format' => [
					'%d',
					'%d',
					'%d',
					'%s',
				]
			];

			$wpdb->insert(
				$wpdb->prefix . self::getTableName(),
				$data['data'],
				$data['format']
			);

			update_user_meta( $user_id,
				'eo_wc_points',
				(int) get_user_meta( $user_id, 'eo_wc_points', true ) - $points );

		} else {
			update_user_meta( $user_id,
				'eo_wc_points',
				0 );
		}


		return true;
	}

	/**
	 * @param $user_id
	 *
	 * @return int
	 */
	public static function getUsersPoints( $user_id ): int {

		global $wpdb;

		$query  = "SELECT SUM(points) FROM " . $wpdb->prefix . self::getTableName() . " WHERE user_id = %d";
		$points = $wpdb->get_var( $wpdb->prepare( $query, $user_id ) );

		if ( $points ) {
			return $points;
		}

		return 0;
	}

	/**
	 * @param $user_id
	 *
	 * @return string
	 */
	public static function getUsersPointsValue( $user_id ) {

		return self::calculatePointsValue( self::getUsersPoints( $user_id ) );
	}

	/**
	 * @param $amount
	 *
	 * @return float|int
	 */
	public static function calculatePoints( $amount ) {
		list( $points, $monetary_value ) = explode( ':', get_option( 'eo_wc_points_earn' ) );

		if ( ! $points ) {
			return 0;
		}

		return round( $amount * ( $points / $monetary_value ) );
	}

	/**
	 * @param $amount
	 *
	 * @return string
	 */
	public static function calculatePointsValue( $amount ) {

		list( $points, $monetary_value ) = explode( ':', get_option( 'eo_wc_points_redeem' ) );

		return number_format( $amount * ( $monetary_value / $points ), 2, '.', '' );
	}

	/**
	 * @param $discount_amount
	 *
	 * @return float
	 */
	public static function calculatePointsForDicsount( $discount_amount ) {

		list( $points, $monetary_value ) = explode( ':', get_option( 'eo_wc_points_redeem' ) );

		$required_points = $discount_amount * ( $points / $monetary_value );

		$required_points = floor( $required_points * 100 );
		$required_points = $required_points / 100;

		return ceil( $required_points );
	}

}
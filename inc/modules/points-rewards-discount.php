<?php namespace EOPlugin\Inc\Modules;

use EOLib\Classes\PluginHook,
	WC_Coupon;

/**
 * Class PointsRewardsDiscount
 * @package EOPlugin\Inc\Modules
 */
class PointsRewardsDiscount {

	/**
	 * PointsRewardsDiscount constructor.
	 */
	public function __construct() {

		//Actions
		$this->addActions();
		$this->addFilters();
	}

	/**
	 * Actions
	 */
	public function addActions() {}

	/**
	 * Filters
	 */
	public function addFilters() {
		// set coupon data
		PluginHook::addFilter( 'woocommerce_get_shop_coupon_data', $this, 'getDiscountData', 10, 2 );

		// filter the message
		PluginHook::addFilter( 'woocommerce_coupon_message', $this, 'getDiscountMessage', 10, 3 );

		// Set the discount
		PluginHook::addFilter( 'woocommerce_coupon_get_discount_amount', $this, 'getDiscountAmount', 10, 5 );
	}

	/**
	 * @param $data
	 * @param $code
	 *
	 * @return array
	 */
	public function getDiscountData( $data, $code ) {

		if ( strtolower( $code ) != $this->getDiscountCode() ) {
			return $data;
		}

		return [
			'id'                         => true,
			'type'                       => 'fixed_cart',
			'amount'                     => 0,
			'coupon_amount'              => 0,
			'individual_use'             => 'no',
			'product_ids'                => '',
			'exclude_product_ids'        => '',
			'usage_limit'                => '',
			'usage_count'                => '',
			'expiry_date'                => '',
			'apply_before_tax'           => 'yes',
			'free_shipping'              => 'no',
			'product_categories'         => array(),
			'exclude_product_categories' => array(),
			'exclude_sale_items'         => 'no',
			'minimum_amount'             => '',
			'maximum_amount'             => '',
			'customer_email'             => ''
		];
	}

	/**
	 * @param $discount
	 * @param $discounting_amount
	 * @param $cart_item
	 * @param $single
	 * @param $coupon
	 *
	 * @return mixed
	 */
	public function getDiscountAmount( $discount, $discounting_amount, $cart_item, $single, $coupon ) {
		if ( strtolower( $coupon->code ) != $this->getDiscountCode() ) {
			return $discount;
		}

		$discount_percent = 0;

		if ( WC()->cart->subtotal_ex_tax ) {
			$discount_percent = ( $cart_item['data']->get_price_excluding_tax() * $cart_item['quantity'] ) / WC()->cart->subtotal_ex_tax;
		}

		$total_discount = PointsCart::getRedeemingPoints();
		$total_discount = $total_discount * $discount_percent;

		return $total_discount;
	}

	/**
	 * @param $message
	 * @param $message_code
	 * @param $coupon
	 *
	 * @return string
	 */
	public function getDiscountMessage( $message, $message_code, $coupon ) {
		if ( $message_code === WC_Coupon::WC_COUPON_SUCCESS && $coupon->code === $this->getDiscountCode() ) {
			return __( 'Discount Applied Successfully', 'eo-wc-points' );
		} else {
			return $message;
		}
	}

	/**
	 * @return string
	 */
	public static function generateDiscountCode() {

		$discount_code = sprintf( 'eo_wc_points_redemption_%s_%s',
			get_current_user_id(), date( 'Y_m_d_h_i',
				current_time( 'timestamp' ) )
		);

		WC()->session->set( 'eo_wc_points_coupon', $discount_code );

		return $discount_code;
	}

	/**
	 * @return array|null|string
	 */
	public static function getDiscountCode() {
		if ( WC()->session !== null ) {
			return WC()->session->get( 'eo_wc_points_coupon' );
		}

		return null;
	}

}

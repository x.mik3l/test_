<?php namespace EOPlugin\Inc\Modules;

use EOLib\Classes\PluginHook,
	WC_Settings_Page;

/**
 * Class PointsSettings
 * @package EOPlugin\Inc\Modules
 */
class PointsSettings extends WC_Settings_Page {

	/**
	 * Constructor
	 */
	public function __construct() {
		parent::__construct();

		$this->id = 'eo_wc_points';
		$this->label = __( 'EO WC Points', 'eo-wc-points' );

		//Actions
		$this->addActions();
		$this->addFilters();
	}

	/**
	 * Init Actions
	 */
	public function addActions() {
		PluginHook::addAction( 'woocommerce_sections_' . $this->id,  $this, 'output_sections' );
		PluginHook::addAction( 'woocommerce_settings_' . $this->id,  $this, 'output' );
		PluginHook::addAction( 'woocommerce_settings_save_' . $this->id,  $this, 'save' );


		// Add a custom field type [eo_wc_conversion]
		PluginHook::addAction( 'woocommerce_admin_field_eo_wc_conversion',  $this, 'renderConversationField' );
	}

	/**
	 * Init Filters
	 */
	public function addFilters() {
		PluginHook::addFilter( 'woocommerce_settings_tabs_array',  $this, 'add_settings_tab', 100 );

		// Add a custom field type [eo_wc_conversion]
		PluginHook::addFilter( 'woocommerce_admin_settings_sanitize_option_eo_wc_points_earn',  $this, 'saveConversationField', 10, 3 );
		PluginHook::addFilter( 'woocommerce_admin_settings_sanitize_option_eo_wc_points_redeem',  $this, 'saveConversationField', 10, 3 );
	}

	/**
	 * Add plugin options tab
	 *
	 * @return array
	 */
	public function add_settings_tab( $settings_tabs ) {
		$settings_tabs[ $this->id ];
		return $settings_tabs;
	}

	/**
	 * Get sections
	 *
	 * @return array
	 */
	public function get_settings( $section = null ) {

		$settings = [
			[
				'title' => __( 'Points Settings', 'eo-wc-points' ),
				'type'  => 'title',
				'id'    => 'eo_wc_points_settings_start'
			],			
			[
				'title'    => __( 'Earn Points Conversion Rate', 'eo-wc-points' ),
				'id'       => 'eo_wc_points_earn',
				'default'  => '1:1',
				'type'     => 'eo_wc_conversion'
			],			
			[
				'title'    => __( 'Redemption Conversion Rate', 'eo-wc-points' ),
				'id'       => 'eo_wc_points_redeem',
				'default'  => '100:1',
				'type'     => 'eo_wc_conversion'
			],
			['type' => 'sectionend', 'id' => 'eo_wc_points_settings_end' ],

		];

		return apply_filters( 'wc_settings_tab_demo_settings', $settings, $section );

	}

	/**
	 * Render the Earn Points/Redeem Points conversion ratio section
	 *
	 * @since 1.0
	 * @param array $field associative array of field parameters
	 */
	public function renderConversationField( $field ) {
		if ( isset( $field['title'] ) && isset( $field['id'] ) ) :

			$ratio = get_option( $field['id'], $field['default'] );

			list( $points, $monetary_value ) = explode( ':', $ratio );

			?>
			<tr valign="top">
				<th scope="row" class="titledesc">
					<label for=""><?php echo wp_kses_post( $field['title'] ); ?></label>
				</th>
				<td class="forminp forminp-text">
					<fieldset>
						<input name="<?php echo esc_attr( $field['id'] . '_points' ); ?>"
						       id="<?php echo esc_attr( $field['id'] . '_points' ); ?>"
						       type="text" style="max-width: 50px;"
						       value="<?php echo esc_attr( $points ); ?>" />&nbsp;

						<?php _e( 'Points', 'eo-wc-points' ); ?>

						<span>&nbsp;&#61;&nbsp;</span>&nbsp;<?php echo get_woocommerce_currency_symbol(); ?>
						<input name="<?php echo esc_attr( $field['id'] . '_monetary_value' ); ?>"
						       id="<?php echo esc_attr( $field['id'] . '_monetary_value' ); ?>"
						       type="text" style="max-width: 50px;"
						       value="<?php echo esc_attr( $monetary_value ); ?>" />
					</fieldset>
				</td>
			</tr>
		<?php

		endif;
	}

	/**
	 * Save the Earn Points/Redeem Points Conversion field
	 *
	 * @param array $field
	 * @return mixed
	 */
	public function saveConversationField( $value, $option, $raw_value ) {
		if ( isset( $_POST[ $option['id'] . '_points' ] ) && ! empty( $_POST[ $option['id'] . '_monetary_value' ] ) )
			return wc_clean( $_POST[ $option['id'] . '_points' ] ) . ':' . wc_clean( $_POST[ $option['id'] . '_monetary_value' ] );
		return false;
	}

}

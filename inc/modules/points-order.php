<?php namespace EOPlugin\Inc\Modules;

use EOLib\Classes\PluginHook,
	EOPlugin\Inc\Models\Point,
	WC_Order;

/**
 * Class PointsOrder
 * @package EOPlugin\Inc\Modules
 */
class PointsOrder {


	/**
	 * Add hooks/filters
	 */
	public function __construct() {

		//Actions
		$this->addActions();
		$this->addFilters();
	}

	/**
	 * Actions
	 */
	public function addActions() {

		PluginHook::addAction( 'woocommerce_payment_complete', $this, 'addPointsEarned' );

		PluginHook::addAction( 'woocommerce_order_status_processing', $this, 'addPointsEarned' );
		PluginHook::addAction( 'woocommerce_order_status_pending_to_completed', $this, 'addPointsEarned' );
		PluginHook::addAction( 'woocommerce_order_status_on-hold_to_completed', $this, 'addPointsEarned' );

		PluginHook::addAction( 'woocommerce_order_status_failed_to_processing', $this, 'addPointsEarned' );
		PluginHook::addAction( 'woocommerce_order_status_failed_to_completed', $this, 'addPointsEarned' );

		PluginHook::addAction( 'woocommerce_checkout_order_processed', $this, 'checkRedeemedPoints' );

		PluginHook::addAction( 'woocommerce_order_status_cancelled', $this, 'cancelledRefundedOrder' );
		PluginHook::addAction( 'woocommerce_order_status_refunded', $this, 'cancelledRefundedOrder' );
		PluginHook::addAction( 'woocommerce_order_status_failed', $this, 'cancelledRefundedOrder' );
	}

	/**
	 * Filters
	 */
	public function addFilters() {}

	/**
	 * @param $order
	 */
	public function addPointsEarned( $order ) {

		if ( ! is_object( $order ) ) {
			$order = new WC_Order( $order );
		}

		if ( ! $order->get_user_id() ) {
			return;
		}

		$points = get_post_meta( $order->get_id(), '_eo_wc_points_earned', true );

		if ( $points >  0) {
			return;
		}

		// get points earned
		$points = $this->getPointsEarnedFoPurchase( $order );

		update_post_meta( $order->get_id(), '_eo_wc_points_earned', $points );

		if ( ! $points ) {
			return;
		}

		// add points
		Point::increasePoints( $order->get_user_id(), $points, $order->get_id() );

		// add order note
		$order->add_order_note( sprintf( __( 'Customer earned %d %s for purchase.', 'eo-wc-point' ), $points, 'Points' ) );
	}

	/**
	 * @param $order_id
	 *
	 * @return bool
	 */
	public function checkRedeemedPoints( $order_id ) {

		$order = new WC_Order( $order_id );

		if ( ! $order->get_user_id() ) {
			return false;
		}

		$discount_code = PointsRewardsDiscount::getDiscountCode();

		if ( ! WC()->cart->has_discount( $discount_code ) ) {
			return false;
		}

		// Get amount of discount
		$discount_amount = 0;

		if ( isset( WC()->cart->coupon_discount_amounts[ $discount_code ] ) ) {
			$discount_amount += WC()->cart->coupon_discount_amounts[ $discount_code ];
		}

		if ( WC()->cart->prices_include_tax && isset( WC()->cart->coupon_discount_tax_amounts[ $discount_code ] ) ) {
			$discount_amount += WC()->cart->coupon_discount_tax_amounts[ $discount_code ];
		}

		$points_redeemed = Point::calculatePointsForDicsount( $discount_amount );

		// deduct points
		Point::decreasePoints( $order->user_id, $points_redeemed , $order->get_id() );

		update_post_meta( $order->get_id(), '_eo_wc_points_redeemed', $points_redeemed );

		// add order note
		$order->add_order_note( sprintf( __( '%d %s redeemed for a %s discount.', 'eo-wc-points' ), $points_redeemed, 'Points', wc_price( $discount_amount ) ) );

		return true;
	}

	/**
	 * @param $order_id
	 */
	public function cancelledRefundedOrder( $order_id ) {

		$order = new WC_Order( $order_id );

		// bail for guest user
		if ( ! $order->get_user_id() )
			return;

		// handle removing any points earned for the order
		$points_earned = get_post_meta( $order->get_id(), '_eo_wc_points_earned', true );

		if ( $points_earned > 0 ) {

			// remove points
			Point::decreasePoints( $order->get_user_id(), $points_earned, $order->get_id() );

			// remove points from order
			delete_post_meta( $order->get_id(), '_eo_wc_points_earned' );

			// add order note
			$order->add_order_note( sprintf( __( '%d %s removed.', 'eo-wc-points' ), $points_earned, 'Points' ) );
		}

		// handle crediting points redeemed for a discount
		$points_redeemed = get_post_meta( $order->get_id(), '_eo_wc_points_redeemed', true );

		if ( $points_redeemed > 0 ) {

			// credit points
			Point::increasePoints( $order->get_user_id(), $points_redeemed, $order->get_id() );

			// remove points from order
			delete_post_meta( $order->get_id(), '_eo_wc_points_redeemed' );

			// add order note
			$order->add_order_note( sprintf( __( '%d %s credited back to customer.', 'eo-wc-points' ), $points_redeemed, 'Points' ) );
		}
	}

	/**
	 * @param WC_Order $order
	 *
	 * @return float|int|mixed
	 */
	public function getPointsEarnedFoPurchase( WC_Order $order ) {

		$points_earned = 0;

		foreach ( $order->get_items() as $item_key => $item ) {

			$product = $order->get_product_from_item( $item );

			if ( ! is_object( $product ) ) {
				continue;
			}

			if ( get_option('woocommerce_prices_include_tax') == 'no' ) {
				$item_price = $order->get_item_subtotal( $item, false, true );
			} else {
				$item_price = $order->get_item_subtotal( $item, true, true );
			}

			$product->set_price( $item_price );

			// Calc points earned
			$points_earned += Point::calculatePoints( $product->get_price() ) * $item['qty'];
		}



		$discount = (int)$order->order_discount + (int) $order->cart_discount;

		$points_earned -= min( Point::calculatePoints( $discount ), $points_earned );

		return $points_earned;
	}

}

<?php namespace EOPlugin\Inc\Modules;

use EOLib\Classes\PluginHook,
	EOPlugin\Inc\Models\Point;

/**
 * Class PointsCart
 * @package EOPlugin\Inc\Modules
 */
class PointsCart {

	/**
	 * @var string
	 */
	public $label = 'Points';


	/**
	 * Add cart/checkout related hooks / filters
	 *
	 * @since 1.0
	 */
	public function __construct() {

		//Actions
		$this->includes();
		$this->addActions();
		$this->addFilters();
	}

	/**
	 * Includes
	 */
	public function includes() {
		new PointsRewardsDiscount();

		//Orders actions
		new PointsOrder();
	}

	/**
	 * Actions
	 */
	public function addActions() {

		PluginHook::addAction( 'woocommerce_before_cart', $this, 'messageInfo', 10 );
		PluginHook::addAction( 'woocommerce_before_cart', $this, 'messagePurchasePoints', 15 );

		PluginHook::addAction( 'woocommerce_before_checkout_form', $this, 'messageInfo', 5 );
		PluginHook::addAction( 'woocommerce_before_checkout_form', $this, 'messagePurchasePoints', 10 );
		PluginHook::addAction( 'woocommerce_before_checkout_form', $this, 'removePoints', 15 );

		// Coupon loading
		PluginHook::addAction( 'woocommerce_cart_loaded_from_session', $this, 'checkCoupon' );
		PluginHook::addAction( 'woocommerce_applied_coupon', $this, 'checkCoupon' );

		//cart page
		PluginHook::addAction( 'wp', $this, 'setDiscount' );
	}

	/**
	 * Filters
	 */
	public function addFilters() {
		PluginHook::addFilter( 'woocommerce_cart_totals_coupon_label', $this, 'getLabel' );
	}

	/**
	 * Remove points
	 */
	public function removePoints() {
		wc_enqueue_js( '$( "body" ).on ( "click", ".woocommerce-remove-coupon", function( e ) {
			jQuery( ".eo-wc-points-earned-message" ).show();
			e.preventDefault();
		} );' );
	}

	/**
	 * Check coupons
	 */
	public function checkCoupon() {
		$ordered_coupons = [];
		$points          = [];

		foreach ( WC()->cart->get_applied_coupons() as $code ) {
			if ( strstr( $code, 'eo_wc_points_redemption_' ) ) {
				$points[] = $code;
			} else {
				$ordered_coupons[] = $code;
			}
		}

		WC()->cart->applied_coupons = array_merge( $ordered_coupons, $points );
	}

	/**
	 * Set discount
	 */
	public function setDiscount() {

		if ( ! is_cart() || ! isset( $_POST['eo_wc_points_submit'] ) ) {
			return;
		}

		if ( WC()->cart->has_discount( PointsRewardsDiscount::getDiscountCode() ) ) {
			return;
		}

		WC()->session->set( 'eo_wc_points_amount', ( ! empty( $_POST['eo_wc_points_amount'] )
			? absint( $_POST['eo_wc_points_amount'] )
			: ''
		) );

		$discount_code = PointsRewardsDiscount::generateDiscountCode();

		WC()->cart->add_discount( $discount_code );
	}

	/**
	 * @return bool|float|int|mixed
	 */
	public static function getRedeemingPoints() {

		$user_points = Point::getUsersPointsValue( get_current_user_id() );

		if ( $user_points <= 0 ) {
			return 0;
		}

		$discount_applied = 0;

		if ( ! did_action( 'woocommerce_before_calculate_totals' ) ) {
			WC()->cart->calculate_totals();
		}

		foreach ( WC()->cart->get_cart() as $item_key => $item ) {

			$discount = 0;

			if ( method_exists( $item['data'], 'get_price_including_tax' ) ) {
				$max_discount = $item['data']->get_price_including_tax( $item['quantity'] );
			} else {
				$max_discount = $item['data']->get_price() * $item['quantity'];
			}

			$discount = ( $user_points <= $max_discount ) ? $user_points : $max_discount;

			$discount_applied += $discount;

			$user_points -= $discount;
		}

		if ( 'no' === get_option( 'woocommerce_prices_include_tax' ) ) {
			$discount_applied = max( 0, min( $discount_applied, WC()->cart->subtotal_ex_tax ) );

		} else {
			$discount_applied = max( 0, min( $discount_applied, WC()->cart->subtotal ) );
		}

		$max_discount = 0;

		if ( false !== strpos( $max_discount, '%' ) ) {
			$max_discount = self::calculateDiscount( $max_discount );
		}

		if ( $max_discount && $max_discount < $discount_applied ) {
			$discount_applied = $max_discount;
		}

		return $discount_applied >= 1 ? $discount_applied : false;
	}

	/**
	 * @param $percentage
	 *
	 * @return float|int
	 */
	public static function calculateDiscount( $percentage ) {

		$percentage = str_replace( '%', '', $percentage ) / 100;

		if ( 'no' === get_option( 'woocommerce_prices_include_tax' ) ) {
			$discount = WC()->cart->subtotal_ex_tax;

		} else {
			$discount = WC()->cart->subtotal;

		}

		return $percentage * $discount;
	}

	/**
	 * @return float|int
	 */
	public function getEarnedPoints() {

		$points_earned = 0;
		foreach ( WC()->cart->get_cart() as $item_key => $item ) {
			$points_earned += $item['data']->get_price() * $item['quantity'];
		}

		$discount        = WC()->cart->discount_cart;
		$discount_amount = min( Point::calculatePoints( $discount ), $points_earned );

		$points_earned = $points_earned - $discount_amount;

		return Point::calculatePoints( $points_earned );
	}

	/**
	 * Message on the cart page
	 */
	public function messageInfo() {

		// get the total points earned for this purchase
		$points_earned = $this->getEarnedPoints();

		if ( ! $points_earned ) {
			return;
		}

		_e( '<div class="eo-wc-points eo-wc-points-earned-message woocommerce-info">Get ' . $points_earned . ' points per purchase</div>',
			'eo-wc-points' );
	}

	/**
	 * Form for apply points
	 */
	public function messagePurchasePoints() {

		if ( ! wc_coupons_enabled() || WC()->cart->has_discount( PointsRewardsDiscount::getDiscountCode() ) ) {
			return;
		}

		$discount_available = $this->getRedeemingPoints();

		if ( ! $discount_available ) {
			return;
		}

		$points  = Point::calculatePointsForDicsount( $discount_available );
		$message = __( "Use your: {$points} points for a " . wc_price( $discount_available ) . "discount on this order!", "eo-wc-points" );

		// add 'Apply Discount' button
		$message .= '<form class="eo-wc-points-form" action="' . wc_get_cart_url() . '" method="post">';
		$message .= '<input type="hidden" name="eo_wc_points_amount" class="eo-wc-points-amount" />';
		$message .= '<input type="submit" class="button eo_wc_points_submit" name="eo_wc_points_submit" value="' . __( 'Apply Discount', 'eo-wc-points' ) . '" /></form>';

		// wrap with info div
		$message = '<div class="woocommerce-info eo-wc-points-message">' . $message . '</div>';

		echo $message;
	}

	/**
	 * @return string
	 */
	public function getLabel() {
		return $this->label;
	}

}


<?php namespace EOPlugin\Inc;

use EOLib\Classes\PluginAdmin;
use EOLib\Classes\PluginHook;

class EoAdmin extends PluginAdmin {

    protected function addActions()
    {
        parent::addActions();
    }

    protected function addFilters()
    {
        parent::addFilters();
    }
}
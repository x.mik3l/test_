<?php namespace EOPlugin\Inc\Controllers;

use EOLib\Classes\Controllers\PluginController,
	EOLib\Classes\PluginHook,
	EOLib\Classes\Debug\PluginDebug,
	EOLib\Classes\Template\PluginTemplate,
	EOPlugin\Inc\Models\Point;

use EOPlugin\Inc\Modules\{
	PointsSettings,
	PointsCart
};


/**
 * Class DisplayPoints
 * @package EOPlugin\Inc\Controllers
 */
class DisplayPoints extends PluginController {

	/**
	 * @var Point $_point
	 */
	private $_point;

	/**
	 * @var PointsSettings $settings
	 */
	private $settings;


	/**
	 * DisplayPoints constructor.
	 */
	public function __construct() {
		parent::__construct();

		try {

			$this->checkCompatible();
			$this->_point = new Point();

		} catch ( \Exception $e ) {
			PluginDebug::writeToLog(
				sprintf( 'Code: %s; Error: %s', $e->getCode(), $e->getMessage() )
			);
		}

	}

	/**
	 * Init actions
	 */
	protected function addActions() {
		parent::addActions();

		// Backend
		if ( is_admin() && ! defined( 'DOING_AJAX' ) ) {
			PluginHook::addAction( 'woocommerce_get_settings_pages', $this, 'addSettingsPage' );
		}

		//Frontend
		PluginHook::addAction( 'woocommerce_before_my_account', $this, 'showPoints' );

		//Cart
		new PointsCart();
	}

	/**
	 * Add EO WC Points settings page to WooCommerce settings
	 *
	 * @internal
	 *
	 * @param array $settings
	 *
	 * @return array
	 */
	public function addSettingsPage( $settings ): array {

		$this->settings = new PointsSettings();
		$settings[]     = $this->settings;

		return $settings;
	}

	/**
	 * Validate function
	 *
	 * @param     $value
	 * @param int $filter
	 *
	 * @return bool|mixed
	 */
	public function getValue( $value, $filter = FILTER_SANITIZE_STRING ) {
		if ( $value ) {
			return filter_var( $value, $filter );
		}

		return false;
	}

	/**
	 * Show points on my account page
	 */
	public function showPoints() {
		PluginTemplate::displayTemplate( 'templates/public/my-account/my-account',
			[
				'user_points' => Point::getUsersPoints( get_current_user_id() )
			]
		);
	}

	/**
	 * @return bool
	 * @throws \Exception
	 */
	public function checkCompatible(): bool {

		if ( version_compare( phpversion(), '5.2.0' ) < 0 ) {
			throw new \Exception( 'php version must be greater than 5.2.0' );
		}

		if ( ! in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
			throw new \Exception( 'WC must be enabled' );
		}

		return true;
	}

	/**
	 * @return Point
	 */
	public function getPoint(): Point {
		return $this->_point;
	}

	/**
	 * @param Point $affiliate
	 */
	public function setPoint( Point $point ): void {
		$this->_point = $point;
	}

}
<?php namespace EOPlugin\Inc;

use EOLib\Classes\PluginPublic;

class EoPublic extends PluginPublic {

    protected function addActions()
    {
        parent::addActions();
    }

    protected function addFilters()
    {
        parent::addFilters();
    }

}
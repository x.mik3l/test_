<?php namespace EOLib;

use EOPlugin\Inc\Models\Point;

/**
 * Class PluginActive
 * @package EOLib
 */
class PluginActive {

	public static function activate() {

		//Affiliate install tables
		Point::updateTable();

		//Update rewrite rules
		flush_rewrite_rules();
	}

}

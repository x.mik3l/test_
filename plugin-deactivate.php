<?php namespace EOLib;

/**
 * Class PluginDeactivate
 * @package EOLib
 */
class PluginDeactivate {

	public static function deactivate() {

		//Remove rewrite rules
		flush_rewrite_rules();
	}

}

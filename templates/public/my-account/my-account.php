<?php
/**
 * My Account - My Points
 */
?>
<?php if ( $user_points ) : ?>

    <h3><?php _e( 'Your points:', 'eo-wc-points' ); ?></h3>

    <p><?php printf( __( "You have %d points", 'eo-wc-points' ), $user_points ); ?></p>

<?php endif; ?>
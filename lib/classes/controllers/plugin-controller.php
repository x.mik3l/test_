<?php namespace EOLib\Classes\Controllers;


class PluginController {

    public function __construct()
    {
        $this->addActions();
        $this->addFilters();
    }

    /**
     * Method used to add actions
     */
    protected function addActions() {

    }

    /**
     * Method used to add filters
     */
    protected function addFilters() {

    }

}


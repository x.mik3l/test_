<?php namespace EOLib\Classes;

use EOLib\Classes\Template\PluginTemplate;
use EOLib\PluginInit;

class Plugin {

    protected static $templatePath = '';

    public function __construct()
    {
        $this->addActions();
        $this->addFilters();
    }

    /**
     * Method used to add actions
     */
    protected function addActions() {

    }

    /**
     * Method used to add filters
     */
    protected function addFilters() {

    }

    /**
     * @param $string
     * @param bool $echo
     * @return mixed|string
     */
    public static function translate($string, $echo = true) {

        if($echo) {
            _e($string, PluginInit::getDomainName());
        } else {
            return __($string, PluginInit::getDomainName());
        }

        return $string;
    }

    public static function getTemplate($path, $parameters = array(), $echo = true) {
        if(PluginInit::getDomainName() . '\Inc\EoAdmin') {
            $parameters['eoAdmin'] = PluginInit::getEoAdminClass();
        } elseif(PluginInit::getDomainName() . '\Inc\EoPublic') {
            $parameters['eoPublic'] = PluginInit::getEoPublicClass();
        }

        $path = 'templates' .'.'. static::$templatePath . '.' . $path;
        return PluginTemplate::displayTemplate($path, $parameters, $echo);
    }

    public static function addStyle($path, $deps = false) {
        $handle = $path;
        $path = 'assets.css.' . static::$templatePath . '.' . $path;
        $path = PluginTemplate::formatPath($path, 'css');
        $path = PluginInit::getPluginUrl() . $path;

        wp_register_style($handle, $path, $deps, PluginInit::getVersion());
        wp_enqueue_style($handle);
    }

}
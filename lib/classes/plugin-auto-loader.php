<?php namespace EOLib\Classes;

use EOLib\PluginInit;

class PluginAutoLoader {

    public static $plugin_namespace_path;

    public static function autoLoader($class) {
        $file = explode('\\', $class);

        if($file[0] == self::getPluginNamespace(false) || $file[0] == PluginInit::getLibNamespace()) {
            $file_path = self::reverseBackslashes($class);
            $file_path = self::formatNamespace($file[0], $file_path);
            $file_path = self::addHyphenBetweenUpperCases($file_path);
            $file_path = self::toLower($file_path);
            $file_path = self::addPluginRootPath($file_path);

            self::requireFile($file_path);
        }
    }

    public static function requireFile($file_path) {
        if(file_exists($file_path . '.php')) {
            require $file_path . '.php';
        }
    }

    public static function addPluginRootPath($path) {
        return PluginInit::getPluginRoot() . $path;
    }

    public static function toLower($path) {
        return strtolower($path);
    }

    public static function addHyphenBetweenUpperCases($path) {
        return preg_replace('/\B([A-Z])/', '-$1', $path);
    }

    public static function formatNamespace($file, $path) {
        // In case of requiring lib files
        if($file == PluginInit::getLibNamespace()) {
            $path = self::replaceNamespace(PluginInit::getLibNamespace(), $path, 'lib');
        } else {
            $path = self::replaceNamespace(self::getPluginNamespace(), $path);
        }

        return $path;
    }

    public static function replaceNamespace($namespace, $path, $replace = '') {
        return str_replace($namespace, $replace, $path);
    }

    public static function reverseBackslashes($class) {
        return str_replace('\\', '/', $class);
    }

    public static function setPluginNamespace($namespace) {
        self::$plugin_namespace_path = $namespace;
    }

    public static function getPluginNamespace($with_ending_slash = true) {
        $namespace = self::$plugin_namespace_path;

        if($with_ending_slash) {
            $namespace .= '/';
        }

        return $namespace;
    }
}
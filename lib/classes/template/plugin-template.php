<?php namespace EOLib\Classes\Template;

use EOLib\Classes\Debug\PluginDebug;
use EOLib\PluginInit;

class PluginTemplate {

    public static function formatPath($path, $extension = 'php') {
        return str_replace('.', '/', $path) . '.' . $extension;
    }

    public static function displayTemplate($path, $parameters = array(), $echo = true) {
        $output = null;
        //$pluginPath = PluginInit::getPluginRoot($path);
        //$path = PluginTemplate::formatPath($pluginPath);
	    $path = PluginInit::getPluginRoot($path) . '.php';

	    if(file_exists($path)) {
            extract($parameters);

            ob_start();
            include $path;
            $output = ob_get_clean();
        } else {
            PluginDebug::writeToLog("Couldn't find the template ($path)");
        }

        if ($echo) {
            print $output;
        }

        return $output;
    }

}
<?php namespace EOLib;

use EOLib\Classes\PluginAutoLoader;
use EOLib\Classes\PluginLoadClass;
use EOPlugin\Inc\EoAdmin;
use EOPlugin\Inc\EoPublic;

class PluginInit {

    public static $eoAdmin;
    public static $eoPublic;
    protected static $pluginRoot;
    protected static $pluginDomain;
    protected static $pluginInfo;

    private $instantiate = array(
        'inc/controllers',
        'inc/post-types',
    );

    public function __construct($namespace, $plugin_root)
    {
        // Essential plugin info
        static::$pluginRoot = $plugin_root;
        $this->setDomainName($namespace);
        $this->instantiate = apply_filters('eo_instantiate_classes', $this->instantiate);

        // Autoloader
        require_once 'classes/plugin-auto-loader.php';
        PluginAutoLoader::setPluginNamespace($namespace);
        spl_autoload_register(__NAMESPACE__ . '\classes\PluginAutoLoader::autoLoader');


        // Plugin File
        $this->activatePluginType();
        $this->autoInstantiate();
    }


    public static function getPluginInfo($key) {
        if(empty(self::$pluginInfo)) {
            self::$pluginInfo = get_plugin_data(static::getPluginRoot('index.php'));
        }

        return array_key_exists($key,  self::$pluginInfo) ?  self::$pluginInfo[$key] : false;
    }

    public static function getVersion()
    {
        return self::getPluginInfo('Version');
    }

    private function getInstantiate() {
        return $this->instantiate;
    }

    private function setDomainName($domain) {
        static::$pluginDomain = $domain;
    }

    public static function getDomainName() {
        return static::$pluginDomain;
    }

    public static function getPluginUrl($url = '') {
        return str_replace('lib/', '', plugin_dir_url(__FILE__)) . $url;
    }

    public static function getPluginRoot($path = '') {
        return str_replace('index.php', '', self::$pluginRoot) . $path;
    }

    public static function getLibNamespace() {
        return __NAMESPACE__;
    }

    private function autoInstantiate() {
        $pluginRoot = self::getPluginRoot();
        $pluginNamespace = PluginAutoLoader::getPluginNamespace(false);
        $includes = $this->getInstantiate();
        $files = array();

        foreach($includes AS $include) {
            $path = $pluginRoot . $include;
            $d_files = array_slice(scandir($path), 3);

            if(!empty($d_files)) {
                foreach ($d_files as $file) {
                    $file_extension = '.' . pathinfo($path . $file, PATHINFO_EXTENSION);

                    if ($file_extension == '.php') {
                        $files[] = $include .'/'. basename($file, $file_extension);
                    }
                }
            }
        }

        if(!empty($files)) {
            foreach($files AS $file) {
                $file = PluginLoadClass::formatPathToClass($file);
                $class =  $pluginNamespace .'\\'. $file;
                new $class;
            }
        }
    }

    private function activatePluginType() {
        if(is_admin()) {
            static::$eoAdmin = new EoAdmin();
        } else {
            static::$eoAdmin = new EoPublic();
        }
    }

    public static function getEoAdminClass() {
        if(!is_object(static::$eoAdmin)) {
            static::$eoAdmin = new EoAdmin();
        }

        return static::$eoAdmin;
    }

    public static function getEoPublicClass() {
        if(!is_object(static::$eoPublic)) {
            static::$eoPublic = new EoPublic();
        }

        return static::$eoPublic;
    }

}
<?php namespace EOPlugin;

use EOLib\PluginInit,
	EOLib\PluginActive,
	EOLib\PluginDeactivate;

/**
 * Plugin Name: EO WC Points
 * Description: The EO WC Points plugin
 * Version: 1.0.0
 * Text Domain: eo-wc-points
 */

// Plugin Folder URL
if ( ! defined( 'EO_WC_POINTS' ) ) {
	define( 'EO_WC_POINTS', plugin_dir_url( __FILE__ ) );
}

register_activation_hook( __FILE__, function () {
	require_once plugin_dir_path( __FILE__ ) . '/plugin-activate.php';
	PluginActive::activate();
} );

register_deactivation_hook( __FILE__, function () {
	require_once plugin_dir_path( __FILE__ ) . '/plugin-deactivate.php';
	PluginDeactivate::deactivate();
} );

if ( ! class_exists( 'EOLib\PluginInit' ) ) {
	require_once 'lib/plugin-init.php';
}

new PluginInit( __NAMESPACE__, __FILE__ );